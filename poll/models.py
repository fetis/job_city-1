import math
from django.core.cache import cache
from django.db import models
from django.db.models import Sum
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver


class PollManager(models.Manager):

    def get_active_polls(self):
        return self.filter(active=True)


class Poll(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)
    active = models.BooleanField(verbose_name='Активен', default=True)
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    multiple_choice = models.BooleanField(verbose_name='Мультивыбор', default=False)

    objects = PollManager()

    def get_results(self):
        key = settings.POLL_ID_RESULT.format(self.pk)
        result = cache.get(key)
        if result:
            return result
        else:
            return self.set_results()

    def set_results(self):
        votes = self.votes.all()
        counts = self.votes.all().aggregate(total_counts=Sum('count'))['total_counts']
        results = {'count': counts, 'votes': list()}
        for vote in votes:
            result = dict()
            if counts:
                result['percent'] = math.floor((vote.count / counts) * 100)
            else:
                result['percent'] = 0
            result['count'] = vote.count
            result['name'] = vote.name
            results['votes'].append(result)
        key = settings.POLL_ID_RESULT.format(self.pk)
        cache.set(key, results, 3600)
        return results

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Опрос'
        verbose_name_plural = 'Опросы'
        ordering = ['-created']


class PollVote(models.Model):
    poll = models.ForeignKey(Poll, verbose_name='Опрос', related_name='votes')
    name = models.CharField(verbose_name='Название', max_length=150)
    count = models.PositiveIntegerField(verbose_name='Счетчик голосов', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Вариант ответа'
        verbose_name_plural = 'Варианты ответов'
        ordering = ['pk']


@receiver(post_save, sender=PollVote)
def save_vote(sender, instance, **kwargs):
    instance.poll.set_results()
