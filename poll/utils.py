import json


def get_votes_cookies(vote_cookie_str):
    try:
        data = json.loads(vote_cookie_str)
        if data and isinstance(data, list):
            return data
    except TypeError:
        pass
    return []