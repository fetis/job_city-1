from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.base import View
from pure_pagination import PaginationMixin

from job.utils import json_response, MenuMixin
from poll.models import Poll, PollVote
from poll.utils import get_votes_cookies


class PollIndexView(MenuMixin, TemplateView):
    template_name = 'polls/poll_index.html'
    menu_slug = 'poll'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        polls = Poll.objects.get_active_polls()
        polls_complete = get_votes_cookies(self.request.COOKIES.get('polls_vote', []))
        if polls_complete:
            active_poll = polls.exclude(pk__in=polls_complete).first()
        else:
            active_poll = polls.first()
        ctx['open_vote'] = active_poll
        return ctx


class AllPollListView(MenuMixin, PaginationMixin, ListView):
    template_name = 'polls/all_polls.html'
    queryset = Poll.objects.get_active_polls().prefetch_related('votes')
    paginate_by = 10
    menu_slug = 'poll'


class VoteAddView(View):

    def post(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        vote_pk = request.POST.getlist('vote')
        polls_cookie = get_votes_cookies(self.request.COOKIES.get('polls_vote', []))
        if pk and vote_pk and pk.isdigit() and pk not in polls_cookie:
            poll = Poll.objects.get_active_polls().filter(pk=pk).first()
            if poll.multiple_choice:
                votes = PollVote.objects.filter(poll=poll, pk__in=vote_pk)
                for vote in votes:
                    vote.count += 1
                    vote.save(update_fields=['count'])
            else:
                vote = PollVote.objects.filter(poll=poll, pk=vote_pk[0]).first()
                if poll and vote:
                    vote.count += 1
                    vote.save(update_fields=['count'])
            data = {'result': True}
            polls_cookie.append(poll.pk)
            response = json_response(data)
            response.set_cookie('polls_vote', polls_cookie)
            return response
        else:
            data = {'result': False}
            return json_response(data)