from django.contrib import admin

from poll.models import PollVote, Poll


class PollVoteInline(admin.StackedInline):
    model = PollVote
    readonly_fields = ['count']


class PollAdmin(admin.ModelAdmin):
    inlines = [PollVoteInline]
    list_display = ['name', 'active']


admin.site.register(Poll, PollAdmin)