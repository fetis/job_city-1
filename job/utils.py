import json

from django.http import HttpResponse


class MenuMixin:
    menu_slug = ''

    def get_menu_slug(self):
        return self.menu_slug

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['menu_slug'] = self.get_menu_slug()
        return ctx


def json_response(data, status_code=200):
    return HttpResponse(json.dumps(data), content_type='application/json', status=status_code)