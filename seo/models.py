from django.db import models


class MetaTag(models.Model):
    path = models.CharField(verbose_name='Путь', unique=True, max_length=300)
    meta_title = models.CharField(verbose_name='Мета тег title', max_length=255)
    meta_description = models.CharField(verbose_name='Мета тег description', max_length=300, null=True, blank=True)

    def __str__(self):
        return self.path

    class Meta:
        verbose_name = 'Мета тег'
        verbose_name_plural = 'Мета теги'
