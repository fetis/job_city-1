from django.contrib import admin

from seo.models import MetaTag


class MetaTagAdmin(admin.ModelAdmin):
    list_display = ['path', 'meta_title']
    search_fields = ['path', 'meta_title', 'meta_description']


admin.site.register(MetaTag, MetaTagAdmin)