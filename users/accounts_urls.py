from django.conf.urls import url, include
from django.views.generic import TemplateView
from registration.backends.hmac.views import ActivationView

from users.forms import RegistrationEmployerForm, RegistrationApplicantForm
from users.views import RegistrationVariation, RegistrationView

urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^register/employer/$',
        RegistrationView.as_view(
            form_class=RegistrationEmployerForm
        ),
        name='registration_register_employer',
        ),
    url(r'^register/applicant/$',
        RegistrationView.as_view(
            form_class=RegistrationApplicantForm
        ),
        name='registration_register_applicant',
        ),
    url(r'^register/variant/$', RegistrationVariation.as_view(), name='registration_variant'),

    url(r'^activate/complete/$',
        TemplateView.as_view(
            template_name='registration/activation_complete.html'
        ),
        name='registration_activation_complete'),

    url(r'^activate/(?P<activation_key>[-:\w]+)/$',
        ActivationView.as_view(),
        name='registration_activate'),
    url(r'^register/complete/$',
        TemplateView.as_view(
            template_name='registration/registration_complete.html'
        ),
        name='registration_complete'),
    url(r'^register/closed/$',
        TemplateView.as_view(
            template_name='registration/registration_closed.html'
        ),
        name='registration_disallowed'),
]