from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail


@shared_task
def subscribe_send_mail(subject, tpl, to_email):
    send_mail(subject, tpl, settings.DEFAULT_FROM_EMAIL, [to_email], html_message=tpl)

