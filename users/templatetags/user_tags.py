from django import template

register = template.Library()


@register.assignment_tag
def get_fav_ad_count(user):
    return user.get_favorite_count()
