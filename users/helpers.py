from django.contrib.auth.mixins import AccessMixin


class ProfileRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or not (request.user.profile_applicant or request.user.profile_employer):
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class OnlyApplicantMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or not request.user.profile_applicant:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class OnlyEmployerMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or not request.user.profile_employer:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
