from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.template import Template, TemplateSyntaxError, Context
from registration.forms import RegistrationForm

from users.models import User, EmployerProfile, ApplicantProfile, SubscribeTemplate


class RegistrationEmployerForm(RegistrationForm):
    phone = forms.CharField(label='Контактный телефон', max_length=20, required=False)
    company = forms.CharField(label='Название компании', max_length=255, required=False)
    contact = forms.CharField(label='Контактное лицо', max_length=255, required=False)
    address = forms.CharField(label='Адрес компании', max_length=300, required=False)

    class Meta(UserCreationForm.Meta):
        model = User
        fields = [
            'email',
            'phone',
            'company',
            'contact',
            'address',
            'password1',
            'password2'
        ]


class RegistrationApplicantForm(RegistrationForm):
    phone = forms.CharField(label='Контактный телефон', max_length=20, required=False)
    first_name = forms.CharField(label='Имя', max_length=255, required=False)
    last_name = forms.CharField(label='Фамилия', max_length=255, required=False)
    third_name = forms.CharField(label='Отчество', max_length=255, required=False)
    birth_day = forms.DateField(label='Дата рождения', required=False)

    class Meta(UserCreationForm.Meta):
        model = User
        fields = [
            'email',
            'phone',
            'first_name',
            'last_name',
            'third_name',
            'birth_day',
            'password1',
            'password2'
        ]


class PhotoForm(forms.Form):
    photo = forms.ImageField()


class EditApplicantProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.required = True

    class Meta:
        model = ApplicantProfile
        fields = ['phone', 'first_name', 'last_name', 'third_name', 'birth_day']


class EditEmployerProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.required = True

    def clean(self):
        raise ValidationError('Редактировать профиль работодателя нельзя')

    class Meta:
        model = EmployerProfile
        fields = ['phone', 'company', 'contact', 'address']


class EmployerProfileFormAdmin(forms.ModelForm):

    def clean_employer_day(self):
        employer_day = self.cleaned_data.get('employer_day')
        if employer_day and EmployerProfile.objects.filter(employer_day=True).exists():
            raise ValidationError('Работодатель дня уже устнаовлен')
        return employer_day

    class Meta:
        model = EmployerProfile
        fields = '__all__'


class SubscribeTeplateAdminForm(forms.ModelForm):
    template = forms.CharField(label='Шаблон', widget=CKEditorWidget())

    def clean_template(self):
        template = self.cleaned_data.get('template')
        try:
            Template(template)
        except TemplateSyntaxError:
            raise ValidationError('Шаблон не валидный')
        return template

    class Meta:
        model = SubscribeTemplate
        fields = '__all__'