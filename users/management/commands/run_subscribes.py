import datetime

from django.core.management.base import BaseCommand

from job_catalog.models import CV, Vacancy
from users.models import Subscribe


class Command(BaseCommand):
    help = 'Рассылка подписки'

    def handle(self, *args, **options):
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        cv_list = CV.objects.filter(created__year=yesterday.year, created__month=yesterday.month,
                                    created__day=yesterday.day, status=2)
        vacancy_list = Vacancy.objects.filter(created__year=yesterday.year, created__month=yesterday.month,
                                              created__day=yesterday.day, status=2)
        for subscribe in Subscribe.objects.filter(subscribe_type=0):
            result = vacancy_list.filter(category=subscribe.category)
            if result.exists():
                subscribe.send_message(result)

        for subscribe in Subscribe.objects.filter(subscribe_type=1):
            result = cv_list.filter(category=subscribe.category)
            if result.exists():
                subscribe.send_message(result)