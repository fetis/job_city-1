from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import TemplateView, UpdateView, ListView, DeleteView
from registration.backends.hmac.views import RegistrationView as HmacRegistrationView

from job_catalog.models import Category
from users.forms import RegistrationApplicantForm, RegistrationEmployerForm, PhotoForm, EditEmployerProfileForm, \
    EditApplicantProfileForm
from users.helpers import ProfileRequiredMixin
from users.models import ApplicantProfile, EmployerProfile, Subscribe


class ProfileView(ProfileRequiredMixin, TemplateView):
    template_name = 'profile/profile_applicant.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.request.user.profile_employer:
            self.template_name = 'profile/profile_employer.html'
            ctx['profile'] = self.request.user.profile_employer
        else:
            ctx['profile'] = self.request.user.profile_applicant
        return ctx


class ProfileChangePhoto(ProfileRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        form = PhotoForm(request.POST, files=request.FILES)
        if form.is_valid() and form.cleaned_data.get('photo'):
            if self.request.user.profile_employer:
                self.request.user.profile_employer.logo = form.cleaned_data['photo']
                self.request.user.profile_employer.save()
            #elif self.request.user.profile_applicant:
                #self.request.user.profile_applicant.photo = form.cleaned_data['photo']
                #self.request.user.profile_applicant.save()
        return HttpResponseRedirect(reverse('profile:profile'))


class RegistrationVariation(TemplateView):
    template_name = 'registration/register_vario.html'


class RegistrationView(HmacRegistrationView):
    redirect_path = ''

    def get(self, request, *args, **kwargs):
        if self.form_class == RegistrationApplicantForm:
            return HttpResponseRedirect(reverse('job_catalog:vacancy_unregister'))
        if self.form_class == RegistrationEmployerForm:
            return HttpResponseRedirect(reverse('job_catalog:cv_unregister'))
        return super(RegistrationView, self).get(request, *args, **kwargs)

    def register(self, form):
        user = super().register(form)
        data = form.cleaned_data
        profile = None
        if isinstance(form, RegistrationApplicantForm):
            """
            profile = ApplicantProfile(phone=data.get('phone'), first_name=data.get('first_name'),
                                       last_name=data.get('last_name'), third_name=data.get('third_name'),
                                       birth_day=data.get('birth_day'), user=user)
            """
            self.redirect_path = reverse('job_catalog:cv_unregister')
        elif isinstance(form, RegistrationEmployerForm):
            """
            profile = EmployerProfile(phone=data.get('phone'), company=data.get('company'), contact=data.get('contact'),
                                      address=data.get('address'), user=user)
            """
            self.redirect_path = reverse('job_catalog:vacancy_unregister')
        profile.save() if profile else None
        return user


class ProfileUpdate(ProfileRequiredMixin, UpdateView):
    template_name = 'profile/profile_edit_applicant.html'
    form_class = EditApplicantProfileForm

    def get_object(self, queryset=None):
        if self.request.user.profile_employer:
            self.form_class = EditEmployerProfileForm
            self.template_name = 'profile/profile_edit_employer.html'
            return self.request.user.profile_employer
        else:
            return self.request.user.profile_applicant

    def get_success_url(self):
        return reverse('profile:profile')


class SubscribeCreateView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        category_pk = request.POST.get('category')
        slug = request.POST.get('slug')
        redirect_path = request.POST.get('redirect')
        subscribe = Subscribe()
        subscribe.user = self.request.user
        if category_pk and category_pk.isdigit():
            category = Category.objects.filter(pk=category_pk).first()
            if category:
                subscribe.category = category
        if slug and slug.isdigit() and int(slug) in [0, 1]:
            subscribe.subscribe_type = int(slug)
        subscribe.save()
        if redirect_path:
            return HttpResponseRedirect(redirect_path)
        return HttpResponseRedirect("/")


class MySubscribesListView(LoginRequiredMixin, ListView):
    template_name = 'profile/my_subscribe_list.html'

    def get_queryset(self):
        return Subscribe.objects.filter(user=self.request.user)


class MySubscribeDeleteView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        qs = Subscribe.objects.filter(user=self.request.user).filter(pk=kwargs['pk'])
        if qs.exists():
            qs.first().delete()
        return HttpResponseRedirect(reverse('profile:my_subscribes'))