from django import template

register = template.Library()


@register.simple_tag
def is_active(ctx_slug, equal_slug):
    return 'active' if ctx_slug == equal_slug else ''
