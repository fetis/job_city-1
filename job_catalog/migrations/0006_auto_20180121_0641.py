# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-21 06:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('job_catalog', '0005_auto_20171229_1053'),
    ]

    operations = [
        migrations.AddField(
            model_name='cv',
            name='in_top',
            field=models.BooleanField(default=False, verbose_name='В топе'),
        ),
        migrations.AddField(
            model_name='vacancy',
            name='in_top',
            field=models.BooleanField(default=False, verbose_name='В топе'),
        ),
    ]
