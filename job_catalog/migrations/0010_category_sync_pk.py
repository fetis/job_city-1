# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-11 16:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('job_catalog', '0009_auto_20180302_1558'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='sync_pk',
            field=models.IntegerField(blank=True, null=True, verbose_name='Ключ'),
        ),
    ]
