# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-04-01 14:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('job_catalog', '0012_auto_20180330_0532'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='address',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='category',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='charge',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='company',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='conditions',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='experience',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='requirements',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='salary_max',
        ),
        migrations.RemoveField(
            model_name='vacancyrequest',
            name='salary_min',
        ),
        migrations.AddField(
            model_name='vacancyrequest',
            name='phone',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Телефон'),
        ),
    ]
