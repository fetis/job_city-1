from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import Paragraph, Spacer
from reportlab.lib.fonts import addMapping

from job_catalog.models import CV, Vacancy


class PdfConstructor:

    def __init__(self, adv):
        self.adv = adv
        self.story = list()

    def create(self):
        if isinstance(self.adv, CV):
           self.processing_for_cv(self.adv)
        elif isinstance(self.adv, Vacancy):
           self.processing_for_vacancy(self.adv)
        else:
            raise Exception('Неизвестный тип объявления')
        return self.story

    def get_styles(self):
        styles = getSampleStyleSheet()
        pdfmetrics.registerFont(TTFont('Times', 'times.ttf', 'UTF-8'))
        pdfmetrics.registerFont(TTFont('Times-Bold', 'timesbd.ttf', 'UTF-8'))
        pdfmetrics.registerFont(TTFont('Times-Italic', 'timesi.ttf', 'UTF-8'))
        pdfmetrics.registerFont(TTFont('Times-BoldItalic', 'timesbi.ttf', 'UTF-8'))
        addMapping('Times', 0, 0, 'Times')
        addMapping('Times', 0, 1, 'Times-Italic')
        addMapping('Times', 1, 0, 'Times-Bold')
        addMapping('Times', 1, 1, 'Times-BoldItalic')
        styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY, fontName='Times', fontSize=11))
        styles.add(ParagraphStyle(name='Justify-Bold', alignment=TA_JUSTIFY, fontName='Times-Bold'))
        styles.add(ParagraphStyle(name='Justify-Bold-Title', alignment=TA_CENTER, fontName='Times-Bold'))
        return styles

    def processing_for_cv(self, adv):
        styles = self.get_styles()
        title = 'Резюме {}'.format(adv.position)
        self.story.append(Paragraph(title, styles["Justify-Bold-Title"]))
        self.story.append(Spacer(1, 12))
        self.story.append(Paragraph(title, styles["Justify-Bold-Title"]))

    def processing_for_vacancy(self, adv):
        styles = self.get_styles()
        title = 'Вакансия {}'.format(adv.title)
        self.story.append(Paragraph(title, styles["Justify-Bold-Title"]))
        self.story.append(Spacer(1, 12))