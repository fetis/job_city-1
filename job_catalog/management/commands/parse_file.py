import traceback

from django.core.management.base import BaseCommand
from django.utils.timezone import now

from job_catalog.models import VacancySync


class Command(BaseCommand):
    help = 'Обработка файлов синхронизации'

    def handle(self, *args, **options):
        sync = VacancySync.objects.filter(status=0).first()
        sync.status = 1
        sync.datetime_sync = now()
        sync.save()
        try:
            sync.parse_file()
        except Exception:
            traceback.print_exc()
            sync.status = 2
            sync.save()