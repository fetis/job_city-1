import datetime

from django.core.management.base import BaseCommand

from job_catalog.models import CV, Vacancy


class Command(BaseCommand):
    help = 'Скрытие топов'

    def handle(self, *args, **options):
        today = datetime.date.today()
        CV.objects.filter(top_end__lte=today).update(top_end=None, in_top=False)
        Vacancy.objects.filter(top_end__lte=today).update(top_end=None, in_top=False)