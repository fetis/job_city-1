import re

import os

from captcha.fields import ReCaptchaField
from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.dispatch import receiver

from users.models import User, EmployerProfile, ApplicantProfile

STATUSES = (
    (0, 'Новая'),
    (1, 'Обновлено'),
    (2, 'Активна'),
    (3, 'Неактивна')
)


class Top(models.Model):
    in_top = models.BooleanField(verbose_name='В топе', default=False)
    top_end = models.DateField(verbose_name='Дата окончания топа', null=True, blank=True)

    class Meta:
        abstract = True


class CategoryManager(models.Manager):

    def get_categories(self):
        return cache.get_or_set(settings.CATEGORIES_LIST, self.all().values('pk', 'name'), 300)


class Category(models.Model):
    name = models.CharField(max_length=300)
    sync_pk = models.IntegerField(verbose_name='Ключ синхронизации', null=True, blank=True)

    objects = CategoryManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Сфера деятельности'
        verbose_name_plural = 'Сферы деятельности'


class VacancyManager(models.Manager):

    def get_vacancy_for_listing(self):
        return self.get_active_vacancies().values('pk', 'title', 'requirements', 'salary_min', 'salary_max',
                                                  'view_count', 'created', 'profile__logo', 'in_top')

    def get_vacancy_count(self):
        count = cache.get(settings.VACANCY_COUNT)
        if not count:
            count = self.get_active_vacancies().count()
            cache.set(settings.VACANCY_COUNT, count, 300)
        return count

    def get_active_vacancies(self):
        return self.filter(status=2, profile__user__is_active=True)


class Vacancy(Top):
    profile = models.ForeignKey(EmployerProfile, verbose_name='Профиль работодателя')
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    experience = models.CharField(max_length=50, verbose_name='Опыт работы', null=True, blank=True)
    charge = models.TextField(verbose_name='Обязанности', null=True, blank=True)
    description = models.TextField(verbose_name='Описание вакансии')
    requirements = models.TextField(verbose_name='Требования', null=True, blank=True)
    conditions = models.TextField(verbose_name='Условия', null=True, blank=True)
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    status = models.IntegerField(verbose_name='Статус', choices=STATUSES, default=0)
    salary_min = models.IntegerField(verbose_name='Зарплата от', null=True, blank=True)
    salary_max = models.IntegerField(verbose_name='Зарплата до', null=True, blank=True)
    category = models.ForeignKey(Category, verbose_name='Сфера деятельности', related_name='cat_vacansy')
    view_count = models.IntegerField(verbose_name='Счетчик просмотра', default=0)
    related_id = models.IntegerField(verbose_name='Id импортированного объявления', null=True, blank=True)

    objects = VacancyManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Вакансии'
        verbose_name = 'Вакансия'


class CVManager(models.Manager):

    def get_cv_count(self):
        count = cache.get(settings.CV_COUNT)
        if not count:
            count = self.get_active_cv().count()
            cache.set(settings.CV_COUNT, count, 300)
        return count

    def get_active_cv(self):
        return self.filter(status=2, profile__user__is_active=True)

    def get_cv_for_listing(self):
        return self.get_active_cv().values('pk', 'position', 'experience', 'desired_salary', 'view_count', 'created',
                                           'profile__photo', 'in_top')


class CV(Top):
    profile = models.ForeignKey(ApplicantProfile, verbose_name='Пользователь', related_name='cv_adverts')
    category = models.ForeignKey(Category, verbose_name='Сфера деятельности', related_name='cat_cv')
    position = models.CharField(verbose_name='Предполагаемая должность', max_length=100)
    desired_salary = models.IntegerField(verbose_name='Желаемая зарплата')
    education = models.CharField(verbose_name='Образование', max_length=300, null=True, blank=True)
    experience = models.TextField(verbose_name='Опыт работы', null=True, blank=True)
    skills = models.TextField(verbose_name='Дополнительные навыки', null=True, blank=True)
    last_work = models.TextField(verbose_name='Последнее место работы', null=True, blank=True)
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)
    status = models.IntegerField(verbose_name='Статус', choices=STATUSES, default=0)
    view_count = models.IntegerField(verbose_name='Счетчик просмотра', default=0)

    objects = CVManager()

    def __str__(self):
        return self.position

    class Meta:
        verbose_name_plural = 'Резюме'
        verbose_name = 'Резюме'


class VacancySync(models.Model):
    datetime_sync = models.DateTimeField(verbose_name='Время синхронизации', null=True, blank=True)
    data_file = models.FileField(verbose_name='Файл синхронизации')
    status = models.PositiveSmallIntegerField(verbose_name='Статус', choices=((0, 'Загружен'), (1, 'В работе'),
                                                                              (2, 'Ошибка'), (3, 'Готово')), default=0)
    log = models.TextField(verbose_name='Лог', null=True, blank=True)

    def __str__(self):
        return self.get_status_display()

    class Meta:
        verbose_name_plural = 'Файлы синхронизации'
        verbose_name = 'Файл синхронизации'

    def parse_file(self):
        def get_email(str):
            emails = re.findall(r'[\w\.-]+@[\w\.-]+', str)
            if emails:
                return emails[0]
            return None

        with open(self.data_file.path, 'r') as file:
            content = file.read()
        results = content.split('\n')
        logs = list()
        bad_counter = 0
        create_counter = 0
        update_counter = 0
        all_counter = 0
        for result in results:
            all_counter += 1
            lines = result.split(';')
            log = {'text': '', 'status': '', 'id': ''}
            if len(lines) >= 3:
                pk = lines[0]
                log['id'] = pk
                vacancy_name = lines[1].replace('"', '')
                description = lines[2].replace('"', '')
                category = lines[-2].replace('"', '')
                email = lines[-1].replace('"', '')
                try:
                    cat_db = Category.objects.get(sync_pk=category)
                except Category.DoesNotExist:
                    log['status'] = 'ошибка'
                    log['text'] = 'Не найдена категория'
                    bad_counter += 1
                    continue
                if not email:
                    email = get_email(description)
                    if not email:
                        bad_counter += 1
                        log['status'] = 'ошибка'
                        log['text'] = 'Не удалось распознать email'
                        continue
                user, created = User.objects.get_or_create(email=email)
                profile, created = EmployerProfile.objects.get_or_create(user=user, defaults={'address': 'Киров'})
                vacancy = Vacancy.objects.filter(related_id=pk)
                data = {'title': vacancy_name, 'description': description, 'category': cat_db}
                if vacancy:
                    vacancy.update(**data)
                    update_counter += 1
                    log['status'] = 'обновлено'
                else:
                    data.update({'profile': profile, 'status': 2, 'related_id': pk})
                    Vacancy.objects.create(**data)
                    create_counter += 1
                    log['status'] = 'создано'
            else:
                bad_counter += 1
                log['status'] = 'ошибка'
                log['text'] = ''
            logs.append(log)
        head = 'Обработано всего: {all};\nОшибок: {errors};\nУспешно обработано: {success};\nОбновлено: {update};\n' \
               'Создано: {create};\n'\
            .format(all=all_counter, errors=bad_counter, success=create_counter + update_counter,
                    update=update_counter, create=create_counter)
        vacancy_text = ''
        for vacancy_log in logs:
            line = 'ID вакансии {id}, статус - {status}, дополнительная информация: {text}\n-----\n'\
                .format(**vacancy_log)
            vacancy_text += line
        self.log = head + vacancy_text
        self.status = 3
        self.save()


class VacancyRequest(models.Model):
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    description = models.TextField(verbose_name='Описание вакансии')
    contact = models.CharField(verbose_name='Контактное лицо', max_length=255, null=True, blank=True)
    phone = models.CharField(verbose_name='Телефон', max_length=20, null=True, blank=True)
    email = models.EmailField(verbose_name='Email работодателя')
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Запрос на создание вакансии'
        verbose_name_plural = 'Запросы на создание вакансий'
        ordering = ('-created', )
    

@receiver(models.signals.post_delete, sender=VacancySync)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    if instance.data_file:
        if os.path.isfile(instance.data_file.path):
            os.remove(instance.data_file.path)
