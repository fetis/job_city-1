import datetime

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

from sorl.thumbnail import ImageField

from job_catalog.models import Category


class Feedback(models.Model):
    company_logo = ImageField(verbose_name='Логотип компании', null=True, blank=True, upload_to='feed_back')
    company_name = models.CharField(verbose_name='Название компании', max_length=200)
    text = RichTextField(verbose_name='Текст отзыва')
    sign = models.CharField(verbose_name='Подпись', max_length=200)
    created = models.DateField(verbose_name='Дата создания', auto_now_add=True)

    def __str__(self):
        return "{} ({})".format(self.sign, self.created.strftime("%d.%m.%Y"))

    class Meta:
        verbose_name = 'Отзыв компании'
        verbose_name_plural = 'Отзывы компаний'


class Partner(models.Model):
    name = models.CharField(verbose_name='Название компании', max_length=200)
    company_logo = ImageField(verbose_name='Логотип компании', upload_to='partners')
    link = models.URLField(verbose_name='Ссылка на сайт', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'


class Education(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=200)
    company_name = models.CharField(verbose_name='Название компании', max_length=255)
    text = RichTextField(verbose_name='Описание')
    phone = models.CharField(verbose_name='Телефон', max_length=20)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)
    active = models.BooleanField(verbose_name='Активен', default=False)

    def __str__(self):
        return self.company_name

    class Meta:
        verbose_name = 'Объявление на обучение'
        verbose_name_plural = 'Объявления на обучение'


class CongratulationsPartner(models.Model):
    name = models.CharField(verbose_name='ФИО', max_length=255)
    sign = models.CharField(verbose_name='Должность, компания и пр.', max_length=255)
    birth_day = models.DateField(verbose_name='Дата рождения')

    def __str__(self):
        return "{} ({})".format(self.name, self.birth_day.strftime("%d.%m.%Y"))

    class Meta:
        verbose_name = 'Поздравление партнера'
        verbose_name_plural = 'Поздравления партнеров'


class Article(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=200)
    cover = ImageField(verbose_name='Обложка', null=True, blank=True, upload_to='articles')
    slug = models.SlugField(verbose_name='Машинное название', max_length=100, unique=True)
    text = RichTextField(verbose_name='Текст')
    published = models.BooleanField(verbose_name='Опубликованно', default=True)
    views = models.PositiveSmallIntegerField(verbose_name='Просмотры', default=0)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)

    def __str__(self):
        return self.title

    def get_previous_article(self):
        return Article.objects.filter(published=True, created__gte=self.created).exclude(pk=self.pk).order_by('pk')\
            .first()

    def get_next_article(self):
        return Article.objects.filter(published=True, created__lte=self.created).exclude(pk=self.pk).order_by('-pk')\
            .first()

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        ordering = ['-created']


class Action(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)
    banner = ImageField(verbose_name='Баннер', upload_to='actions')
    published = models.BooleanField(verbose_name='Опубликован', default=True)
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'
        ordering = ['-created']


class QAJuristManager(models.Manager):

    def get_published_qa(self):
        return self.filter(published=True, answer__isnull=False)

    def get_year_answers(self, year):
        qs = self.get_published_qa()
        return qs.filter(created__year=year)

    def get_years(self):
        qs = self.get_published_qa()
        return qs.dates('created', 'year', order='DESC')

    def get_qa_current_year(self):
        today = datetime.date.today()
        return self.get_year_answers(today.year).values('created', 'question', 'pk')


class QAJurist(models.Model):
    question = models.TextField(verbose_name='Вопрос')
    image = ImageField(verbose_name='Картинка', null=True, blank=True, upload_to='qa')
    answer = models.TextField(verbose_name='Ответ', null=True, blank=True)
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    published = models.BooleanField(verbose_name='Опубликован', default=True)

    objects = QAJuristManager()

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'Вопрос юристу'
        verbose_name_plural = 'Вопросы юристу'
        ordering = ['-created']


class NewsPaper(models.Model):
    name = models.CharField(verbose_name='Название', max_length=100)
    preview = ImageField(verbose_name='Превью', upload_to='news_paper_preview')
    paper_file = models.FileField(verbose_name='Файл газеты', upload_to='news_paper_files')
    date_release = models.DateField(verbose_name='Дата публикации')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Выпуск газеты'
        verbose_name_plural = 'Выпуски газет'
        ordering = ['-date_release']


class PriceList(models.Model):
    name = models.CharField(verbose_name='Название прайса', max_length=100)
    preview = ImageField(verbose_name='Превью прайса', upload_to='price_list_preview')
    price_file = models.FileField(verbose_name='Файл прайса')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Прайс'
        verbose_name_plural = 'Прайсы'


class News(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=200)
    cover = ImageField(verbose_name='Обложка', null=True, blank=True, upload_to='news')
    slug = models.SlugField(verbose_name='Машинное название', max_length=100, unique=True)
    text = RichTextUploadingField(verbose_name='Текст')
    published = models.BooleanField(verbose_name='Опубликованно', default=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['-created']


class SalaryStatManager(models.Manager):

    def get_years(self):
        return self.all().dates('period', 'year', order='DESC')

    def search(self, year, category=None, profession=None):
        qs = self.filter(period__year=year)
        if category:
            qs = qs.filter(category=category)
        if profession:
            qs = qs.filter(profession__icontains=profession)
        return qs


class SalaryStat(models.Model):
    period = models.DateField(verbose_name='Год')
    category = models.ForeignKey(Category, verbose_name='Сфера деятельности', related_name='cat_stat')
    chart = ImageField(verbose_name='График', upload_to='stat_charts')
    profession = models.CharField(verbose_name='Профессия', max_length=255)

    objects = SalaryStatManager()

    def __str__(self):
        return self.profession

    class Meta:
        verbose_name = 'Статистика зарплат'
        verbose_name_plural = 'Статистика зарплат'
        ordering = ['-period']


class ExpertNote(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    slug = models.SlugField(verbose_name='Машинное название', max_length=100, unique=True)
    text = RichTextField(verbose_name='Текст')
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Мнение эксперта'
        verbose_name_plural = 'Мнение эксперта'
        ordering = ['-created']


class Event(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=200)
    cover = ImageField(verbose_name='Обложка', null=True, blank=True, upload_to='events')
    slug = models.SlugField(verbose_name='Машинное название', max_length=100, unique=True)
    text = RichTextField(verbose_name='Текст')
    published = models.BooleanField(verbose_name='Опубликованно', default=True)
    created = models.DateTimeField(verbose_name='Создано', auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Событие'
        verbose_name_plural = 'События'
        ordering = ['-created']