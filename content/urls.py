from django.conf.urls import url

from content.views import FeedBackListView, PartnerListView, EducationAdvertsListView, BirthDaysView, ArticleListView, \
    ArticleDetailView, ActionListView, ContactView, QAJuristCreateView, QAJuristDetail, QAJuristQuestionAjax, \
    NewsPaperList, PriceListView, NewsList, NewsDetail, SalaryStatList, ExpertNoteListView, ExpertNoteDetailView, \
    EventList, EventDetail

urlpatterns = [
    url(r'^feedback/$', FeedBackListView.as_view(), name='feedback'),
    url(r'^partners/$', PartnerListView.as_view(), name='partners'),
    url(r'^education/$', EducationAdvertsListView.as_view(), name='education'),
    url(r'^partner_birth_day/$', BirthDaysView.as_view(), name='birth_day'),
    url(r'^articles/$', ArticleListView.as_view(), name='articles'),
    url(r'^article/(?P<slug>[-\w]+)/$', ArticleDetailView.as_view(), name='article'),
    url(r'^actions/$', ActionListView.as_view(), name='actions'),
    url(r'^contacts/$', ContactView.as_view(), name='contacts'),
    url(r'^qa/$', QAJuristDetail.as_view(), name='qa_detail'),
    url(r'^qa/(?P<pk>\d+)/$', QAJuristDetail.as_view(), name='qa_detail'),
    url(r'^qa_detail/(?P<year>\d+)/$', QAJuristQuestionAjax.as_view(), name='qa_year'),
    url(r'^qa_create/$', QAJuristCreateView.as_view(), name='qa_create'),
    url(r'^archive/$', NewsPaperList.as_view(), name='archive'),
    url(r'^prices/$', PriceListView.as_view(), name='prices'),
    url(r'^news/$', NewsList.as_view(), name='news_list'),
    url(r'^news/(?P<slug>[-\w]+)/$', NewsDetail.as_view(), name='news'),
    url(r'^salary_stats/(?P<year>\d+)/$', SalaryStatList.as_view(), name='stat_list_year'),
    url(r'^salary_stats/$', SalaryStatList.as_view(), name='stat_list'),
    url(r'^mnenie-eksperta/$', ExpertNoteListView.as_view(), name='expert_note_list'),
    url(r'^mnenie-eksperta/(?P<slug>[-\w]+)/$', ExpertNoteDetailView.as_view(), name='expert_note'),
    url(r'^events/$', EventList.as_view(), name='events'),
    url(r'^events/(?P<slug>[-\w]+)/$', EventDetail.as_view(), name='event'),
]