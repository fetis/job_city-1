from ckeditor.widgets import CKEditorWidget
from django import forms

from content.models import Feedback, QAJurist


class FeedBackAdminForm(forms.ModelForm):
    text = forms.CharField(label='Текст отзыва', widget=CKEditorWidget())

    class Meta:
        model = Feedback
        fields = '__all__'


class EducationAdminForm(forms.ModelForm):
    text = forms.CharField(label='Текст', widget=CKEditorWidget())

    class Meta:
        model = Feedback
        fields = '__all__'


class QAJuristAdminForm(forms.ModelForm):
    answer = forms.CharField(label='Ответ', widget=CKEditorWidget())

    class Meta:
        model = QAJurist
        fields = '__all__'
