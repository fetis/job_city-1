from django.urls import reverse
from watson import search as watson
from django.apps import AppConfig


class FeedbackSearchAdapter(watson.SearchAdapter):

    def get_title(self, obj):
        return "Отзыв компании {}".format(obj.company_name)

    def get_description(self, obj):
        return obj.company_name

    def get_content(self, obj):
        return obj.text

    def get_url(self, obj):
        return reverse('content:feedback')


class PartnerSearchAdapter(watson.SearchAdapter):

    def get_title(self, obj):
        return "Наш партнер {}".format(obj.name)

    def get_url(self, obj):
        return reverse('content:partners')


class EducationSearchAdapter(watson.SearchAdapter):

    def get_title(self, obj):
        return "{} в разделе обучение".format(obj.title)

    def get_url(self, obj):
        return reverse('content:education')

    def get_description(self, obj):
        return obj.text


class ArticleSearchAdapter(watson.SearchAdapter):

    def get_title(self, obj):
        return "{} в разделе статьи".format(obj.title)

    def get_url(self, obj):
        return reverse('content:article', kwargs={'slug': obj.slug})

    def get_description(self, obj):
        return obj.text


class NewsSearchAdapter(watson.SearchAdapter):

    def get_title(self, obj):
        return "{} в разделе новости".format(obj.title)

    def get_url(self, obj):
        return reverse('content:news', kwargs={'slug': obj.slug})

    def get_description(self, obj):
        return obj.text


class ContentAppConfig(AppConfig):
    name = 'content'
    verbose_name = 'Контент'

    def ready(self):
        Feedback = self.get_model("Feedback")
        Partner = self.get_model("Partner")
        Education = self.get_model("Education")
        Article = self.get_model("Article")
        News = self.get_model("News")
        watson.register(Feedback, FeedbackSearchAdapter)
        watson.register(Partner, PartnerSearchAdapter)
        watson.register(Education, EducationSearchAdapter)
        watson.register(Article, ArticleSearchAdapter)
        watson.register(News, NewsSearchAdapter)