from django.contrib import admin

from content.forms import FeedBackAdminForm, EducationAdminForm, QAJuristAdminForm
from content.models import Feedback, Partner, Education, CongratulationsPartner, Article, Action, QAJurist, NewsPaper, \
    PriceList, News, SalaryStat, ExpertNote, Event


class FeedBackAdmin(admin.ModelAdmin):
    form = FeedBackAdminForm


class EducationAdmin(admin.ModelAdmin):
    list_display = ['title', 'active', 'company_name']
    form = EducationAdminForm


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'published', 'created']
    readonly_fields = ['views']
    prepopulated_fields = {"slug": ("title",)}


class NewsAdmin(admin.ModelAdmin):
    list_display = ['title', 'published', 'created']
    prepopulated_fields = {"slug": ("title",)}


class ActionAdmin(admin.ModelAdmin):
    list_display = ['name', 'published', 'created']


class NewsPaperAdmin(admin.ModelAdmin):
    list_display = ['name', 'date_release']


class QAJuristAdmin(admin.ModelAdmin):
    form = QAJuristAdminForm


class ExpertNoteAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Feedback, FeedBackAdmin)
admin.site.register(Partner)
admin.site.register(Education, EducationAdmin)
admin.site.register(CongratulationsPartner)
admin.site.register(Article, ArticleAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Action, ActionAdmin)
admin.site.register(QAJurist, QAJuristAdmin)
admin.site.register(NewsPaper, NewsPaperAdmin)
admin.site.register(PriceList)
admin.site.register(SalaryStat)
admin.site.register(ExpertNote, ExpertNoteAdmin)
admin.site.register(Event, EventAdmin)