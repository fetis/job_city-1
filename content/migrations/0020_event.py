# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-13 16:13
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0019_auto_20180307_1531'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок')),
                ('cover', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='events', verbose_name='Обложка')),
                ('slug', models.SlugField(max_length=100, unique=True, verbose_name='Машинное название')),
                ('text', ckeditor.fields.RichTextField(verbose_name='Текст')),
                ('published', models.BooleanField(default=True, verbose_name='Опубликованно')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'Событие',
                'verbose_name_plural': 'События',
                'ordering': ['-created'],
            },
        ),
    ]
