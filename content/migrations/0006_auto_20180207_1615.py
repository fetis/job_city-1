# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-07 16:15
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0005_congratulationspartner'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок')),
                ('cover', sorl.thumbnail.fields.ImageField(blank=True, null=True, upload_to='articles', verbose_name='Обложка')),
                ('text', ckeditor.fields.RichTextField(verbose_name='Текст')),
                ('published', models.BooleanField(default=True, verbose_name='Опубликованно')),
                ('views', models.PositiveSmallIntegerField(default=0, verbose_name='Просмотры')),
                ('created', models.DateField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name_plural': 'Статьи',
                'verbose_name': 'Статья',
                'ordering': ['-created'],
            },
        ),
        migrations.AlterModelOptions(
            name='congratulationspartner',
            options={'verbose_name': 'Поздравление партнера', 'verbose_name_plural': 'Поздравления партнеров'},
        ),
    ]
