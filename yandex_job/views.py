import xml.etree.ElementTree as xml

import datetime
from io import BytesIO

from django.http import HttpResponse
from django.urls import reverse
from django.views import View

from job_catalog.models import Vacancy


class YandexJobGate(View):

    def get(self, request, *args, **kwargs):
        queryset = self.get_vacancies()
        xml_tree = self.generate_xml(queryset)
        f = BytesIO()
        xml_tree.write(f, encoding='utf-8', xml_declaration=True)
        return HttpResponse(f.getvalue(), content_type="application/xml")

    def get_vacancies(self):
        return Vacancy.objects.get_active_vacancies()

    def generate_xml(self, qs):
        attrs = dict()
        attrs['creation-time'] = datetime.datetime.now().isoformat()
        attrs['host'] = 'http://kadry-goroda.ru'
        source = xml.Element('source', attrib=attrs)
        vacancies = xml.SubElement(source, 'vacancies')
        for vacancy in qs:
            vacancies.append(self.generation_xml_item(vacancy))
        tree = xml.ElementTree(source)
        return tree

    def generation_xml_item(self, vacancy):
        vacancy_el = xml.Element('vacancy')

        url = xml.SubElement(vacancy_el, 'url')
        url.text = 'http://kadry-goroda.ru?vacancy={}'.format(vacancy.pk)

        creation_date = xml.SubElement(vacancy_el, 'creation-date')
        creation_date.text = vacancy.created.isoformat()

        if vacancy.salary_max or vacancy.salary_min:
            salary = xml.SubElement(vacancy_el, 'salary')
            if vacancy.salary_max and vacancy.salary_min:
                salary.text = '{} - {}'.format(vacancy.salary_min, vacancy.salary_max)
            elif vacancy.salary_max:
                salary.text = 'до {}'.format(vacancy.salary_max)
            elif vacancy.salary_min:
                salary.text = 'от {}'.format(vacancy.salary_min)

        currency = xml.SubElement(vacancy_el, 'currency')
        currency.text = 'RUR'

        category = xml.SubElement(vacancy_el, 'category')
        industry = xml.SubElement(category, 'industry')
        industry.text = vacancy.category.name

        job_name = xml.SubElement(vacancy_el, 'job-name')
        job_name.text = vacancy.title

        if vacancy.charge:
            duty = xml.SubElement(vacancy_el, 'duty')
            duty.text = vacancy.charge

        if vacancy.conditions:
            term = xml.SubElement(vacancy_el, 'term')
            term_text = xml.SubElement(term, 'text')
            term_text.text = vacancy.conditions

        if vacancy.requirements:
            requirement = xml.SubElement(vacancy_el, 'requirement')
            qualification = xml.SubElement(requirement, 'qualification')
            qualification.text = vacancy.requirements

        addresses = xml.SubElement(vacancy_el, 'addresses')
        address = xml.SubElement(addresses, 'address')
        location = xml.SubElement(address, 'location')
        location.text = vacancy.profile.address

        company = xml.SubElement(vacancy_el, 'company')
        name = xml.SubElement(company, 'name')
        name.text = vacancy.profile.company
        email = xml.SubElement(company, 'email')
        email.text = vacancy.profile.user.email

        #if vacancy.profile.logo:
        #    logo = xml.SubElement(company, 'logo')
        #    logo.text = 'http://kadry-goroda.ru/media/{}'.format(vacancy.profile.logo)
        if vacancy.profile.phone:
            phone = xml.SubElement(company, 'phone')
            phone.text = vacancy.profile.phone
        hr_agency = xml.SubElement(company, 'hr-agency')
        hr_agency.text = 'false'
        #if vacancy.profile.contact:
        #    contact_name = xml.SubElement(company, 'contact-name')
        #    contact_name.text = vacancy.profile.contact
        return vacancy_el

